<?php

namespace Multoo\Twig;

class Engine
{

    /**
     *
     * @var \Twig_Loader_Array
     */
    protected static $twigLoader = null;

    /**
     * @var \Twig_Environment
     */
    protected static $twigEnvironment;
    protected static $addFunction = [];
    protected static $addFilter = [];
    protected static $addExtension = [];

    public static function validate($content)
    {
        self::init();
        self::$twigEnvironment->parse(self::$twigEnvironment->tokenize($content));
    }

    public static function addExtension(\Twig\Extension\AbstractExtension $extension)
    {
        if (is_object(self::$twigEnvironment)) {
            self::$twigEnvironment->addExtension($extension);
        }

        self::$addExtension[] = $extension;
    }

    public static function addFilter($fakeFunctionName, $realFunctionName)
    {
        if (isset(self::$addFilter[$fakeFunctionName]) && self::$addFilter[$fakeFunctionName] !== $realFunctionName) {
            throw new \Exception('Can\'t add filter \'' . $fakeFunctionName . '\' twice to Multoo\Twig\Engine');
        }

        if (is_object(self::$twigEnvironment)) {
            self::$twigEnvironment->addFilter(new \Twig\TwigFilter($fakeFunctionName, $realFunctionName));
        }

        self::$addFilter[$fakeFunctionName] = $realFunctionName;
    }

    public static function addFunction($fakeFunctionName, $realFunctionName, array $options = [])
    {
        if (isset(self::$addFunction[$fakeFunctionName]) && self::$addFunction[$fakeFunctionName][0] !== $realFunctionName) {
            throw new \Exception('Can\'t add function \'' . $fakeFunctionName . '\' twice to Multoo\Twig\Engine');
        }

        if (is_object(self::$twigEnvironment)) {
            self::$twigEnvironment->addFunction(new \Twig\TwigFunction($fakeFunctionName, $realFunctionName, $options));
        }

        self::$addFunction[$fakeFunctionName] = [$realFunctionName, $options];
    }

    protected static function init()
    {
        if (!is_object(self::$twigEnvironment)) {
            if (!is_object(self::$twigLoader)) {
                $locations = ['templates', 'src/views'];
                $fileLoader = new \Twig\Loader\FilesystemLoader($locations, __DIR__ . '/../../../../');

                $arrayLoader = new \Twig\Loader\ArrayLoader([]);

                self::$twigLoader = new \Twig\Loader\ChainLoader([$fileLoader, $arrayLoader]);
            }

            self::$twigEnvironment = new \Twig\Environment(self::$twigLoader);
            self::loadFunctionsAndFilters(self::$twigEnvironment);
        }
    }

    public static function loadFunctionsAndFilters(\Twig\Environment $twig)
    {
        $twig->addExtension(new \Twig_Extensions_Extension_Text());

        foreach (self::$addExtension as $extension) {
            $twig->addExtension($extension);
        }

        foreach (self::$addFunction as $fakeFunctionName => [$realFunctionName, $options]) {
            $twig->addFunction(new \Twig\TwigFunction($fakeFunctionName, $realFunctionName, $options));
        }

        foreach (self::$addFilter as $fakeFunctionName => $realFunctionName) {
            $twig->addFilter(new \Twig\TwigFilter($fakeFunctionName, $realFunctionName));
        }

        $twig->addFilter(new \Twig\TwigFilter('html', function ($value) {
            return html_entity_decode($value, ENT_QUOTES);
        }, array('is_safe' => array('html'))));
    }


    public static function addArrayOfTemplates($array)
    {
        if (!is_object(self::$twigLoader)) {
            self::$twigLoader = new \Twig\Loader\ArrayLoader($array);
        } else {
            foreach ($array as $name => $template) {
                self::$twigLoader->setTemplate($name, $template);
            }
        }
    }

    public static function render($template, $vars)
    {
        self::init();
        return self::$twigEnvironment->render($template, $vars);
    }

    /**
     * @return \Twig_Environment
     */
    public static function getTwigEnvironment(): \Twig\Environment
    {
        self::init();
        return self::$twigEnvironment;
    }

    public static function addGlobal($field, $value)
    {
        self::init();
        self::$twigEnvironment->addGlobal($field, $value);
    }


}
